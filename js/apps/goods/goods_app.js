define(["app", "apps/goods/list/list_controller"], function(ShopManager, ListController) {
  ShopManager.module('GoodsApp', function(GoodsApp, ShopManager, Backbone, Marionette, $, Customjs, _){
    GoodsApp.Router = Marionette.AppRouter.extend({
      appRoutes: {
        "list-goods" : "listGoods",
        "brick-goods" : "brickGoods"
      }
    });

    var API = {
      listGoods: function(){
        var listController = new ListController({mainRegion: ShopManager.mainRegion, list: true});
        listController.show();
      },
      brickGoods: function(){
        var listController = new ListController({mainRegion: ShopManager.mainRegion, list: false});
        listController.show();
      }
    }

    ShopManager.on("goods:list", function() {
      ShopManager.navigate("list-goods");
      API.listGoods();
    });

    ShopManager.on("goods:brick", function() {
      ShopManager.navigate("brick-goods");
      API.brickGoods();
    });

    ShopManager.addInitializer(function(){
      new GoodsApp.Router({
        controller: API
      });
    });


  });
  return ShopManager.GoodsApp;
});
