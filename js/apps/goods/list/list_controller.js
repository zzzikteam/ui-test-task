define(["app", "apps/goods/list/list_view"], function(ShopManager, View) {
  ShopManager.module('GoodsApp.List', function(List, ShopManager, Backbone, Marionette, $, _){
    List.Controller = Marionette.Controller.extend({
      initialize: function(options){
        this.mainRegion = options.mainRegion;
        this.list = options.list;
      },

      _getLayout: function() {
        var layout = new View.Layout();
        this.listenTo(layout, "render", function() {
          this._showMenuAndContent(layout);
        }, this);
        return layout;
      },

      _showMenuAndContent: function(layout) {
        this._initialContent(layout.goodsRegion, layout.loadRegion);
      },


      _initialContent: function(region, upload){
        that = this;
        require(["common/views", "entities/good"], function(CommonViews) {
          var loadingView = new CommonViews.Loading();
	  View.Upload;
          var fetchingGoods = ShopManager.request("good:entities");
          $.when(fetchingGoods).done(function(goods){
            that.goods = goods;
            var ItemView = View.Good;
            var ClassName = "catalog-list";
            var view = new View.Goods({
              itemView: ItemView,
              tagName: "section",
              collection: goods,
              className: ClassName
            });
            

            region.show(view);
          });
        });
      },

      show: function() {
        var layout = this._getLayout();
        this.mainRegion.show(layout);
      },
    });
  });
  return ShopManager.GoodsApp.List.Controller;
});
