var yr = yr || require('yate/lib/runtime.js');

(function() {

    var cmpNN = yr.cmpNN;
    var cmpSN = yr.cmpSN;
    var nodeset2xml = yr.nodeset2xml;
    var nodeset2boolean = yr.nodeset2boolean;
    var nodeset2attrvalue = yr.nodeset2attrvalue;
    var nodeset2scalar = yr.nodeset2scalar;
    var scalar2attrvalue = yr.scalar2attrvalue;
    var xml2attrvalue = yr.xml2attrvalue;
    var scalar2xml = yr.scalar2xml;
    var xml2scalar = yr.xml2scalar;
    var simpleScalar = yr.simpleScalar;
    var simpleBoolean = yr.simpleBoolean;
    var selectNametest = yr.selectNametest;
    var closeAttrs = yr.closeAttrs;

    var M = new yr.Module();

    var j0 = [ ];

    var j1 = [ 0, 'id' ];

    var j2 = [ 0, 'name' ];

    var j3 = [ 0, 'url' ];

    var j4 = [ 0, 'model' ];

    var j5 = [ 0, 'desc' ];

    var j6 = [ 0, 'rating_numb' ];

    var j7 = [ 0, 'price' ];

    var j8 = [ 0, 'src_photo' ];

    var j9 = [ 0, 'inbasket' ];

    var j10 = [ 0, 'brand' ];

    var j11 = [ 0, 'isnew' ];

    var j12 = [ 0, 'delivery_days' ];

    var j13 = [ 0, 'awards' ];

    var j14 = [ 0, 'properties' ];

    var j15 = [ 0, 'colors' ];

    // match /
    M.t0 = function t0(m, c0, i0, l0, a0) {
        var r0 = '';
        var current = [ c0 ];

        r0 += closeAttrs(a0);
        r0 += "<ul>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('id', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('name', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('url', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('model', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('desc', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('rating_numb', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('price', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('src_photo', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('rating_numb', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('price', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('src_photo', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('inbasket', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('brand', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('isnew', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('delivery_days', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('awards', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('properties', c0, []) ) ) + "\"" + "</li>";
        r0 += "<li>" + "\"" + nodeset2xml( ( selectNametest('colors', c0, []) ) ) + "\"" + "</li>";
        r0 += "</ul>";

        return r0;
    };
    M.t0.j = 1;
    M.t0.a = 1;

    M.matcher = {
        "": {
            "": [
                "t0"
            ]
        }
    };
    M.imports = [];

    yr.register('item', M);

})();
