define(["app", 
        "apps/goods/list/templates/layout",
        "apps/goods/list/templates/list",
        "apps/goods/list/templates/item",
], function(ShopManager, layout, list, item) {
  ShopManager.module('GoodsApp.List.View', function(View, ShopManager, Backbone, Marionette, $, Customjs, _){


    Marionette.Renderer.render = function (template, data) {
       var res;
       res = yr.run(template,data);
       return res;
    };


    View.Layout = Marionette.Layout.extend({
      template: 'layout',

      regions: {
        panelRegion: "#panel-region",
        goodsRegion: "#goods-region",
        loadRegion: ".upload",
      }
    });


    View.BaseGood = Marionette.ItemView.extend({
      tagName: "article",

    });

    View.Good = View.BaseGood.extend({
      template: 'item',
    });


    View.Goods = Marionette.CompositeView.extend({
      template: 'list',
      className: "catalog-list",
      itemView: View.Good,
      itemViewContainer: "",
    });
  });
  return ShopManager.GoodsApp.List.View;
});
