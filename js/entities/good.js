define(["app"], function(ShopManager) {
  ShopManager.module("Entities", function(Entities, ShopManager, Backbone, Marionette, $, _) {
    Entities.Good = Backbone.Model.extend({
      urlRoot: "/shop/catalog/goods",
    });

    Entities.GoodCollection = Backbone.Collection.extend({
      url: "/shop/catalog/goods",
      model: Entities.Good,
      comparator: "name",
    });

    var initializeGoods = function() {
      var goods = new Entities.GoodCollection([
        {
          id: 100001,
          name: "Apple iPod touch 5 32GB Blue",
          url: "#apple_ipod_touch_5_32gb_blue",
          model: "Музыкальный плеер",
          desc: "iPod touch — это игровая приставка, музыкальный и видеоплеер, фотоальбом, карманный компьютер, видеокамера и фотоаппарат.",
          rating_numb: 4.9,
          price: 4000,
          src_photo: "img/img.5.jpg",
          rating_numb: 4.2,
          price: 1499000,
          src_photo: "img/img.6.jpg",
          inbasket: 0,
          brand: 'Apple',
          isnew: 1,
          delivery_days: 5,
          awards: {
            reddot: 1,
            eisa: 1,
            tipa: 1
          },
          properties: {
            a: 'Алюминиевый корпус',
            b: 'Retina-дисплей диагональю 4"',
            c: 'Камера 5 Мп',
            d: 'Беспроводной интернет по WiFi'
          },
          colors: {
            back: 1,
            red: 0,
            blue: 0,
            white: 0,
            green: 0
          },
        },
        {
          id: 100002,
          name: "Nikon D7000 Body", 
          url: "#nikon_d7000_body", 
          model: "Зеркальный фотоаппарат",
          desc: "Новая фотокамера Nikon D7000 выполнит любой каприз!",
          rating_numb: 4.2,
          price: 1499000,
          src_photo: "img/img.6.jpg",
          inbasket: 0,
          brand: 'Nikon',
          isnew: 1,
          delivery_days: 5,
          awards: {
            reddot: 1,
            eisa: 0,
            tipa: 0
          },
          properties: {
            a: 'Матрица 16.9 МП (APS-C)',
            b: 'Съемка видео Full HD',
            c: 'Экран 3"',
            d: 'Вес камеры 780 г'
          },
          colors: {
            back: 1,
            red: 0,
            blue: 0,
            white: 0,
            green: 0
          },
        },
        {
          id: 100003,
          name: "National Geographic NG A2540", 
          url: "#national_geographic_ng_a2540", 
          model: "Сумка для фотоаппарата",
          desc: "сумка-ранец Africa NG A2540 — это повседневная функциональная наплечная сумка, которая вмещает в себя все ваше персональное оборудование вместе с камерой или камкордером в специальных защитных отсеках.",
          rating_numb: 1.5,
          price: 7,
          src_photo: "img/img.7.jpg",
          inbasket: 0,
           brand: 'NG',
          isnew: 1,
          delivery_days: 2,
          awards: {
            reddot: 1,
            eisa: 0,
            tipa: 0
          },
          properties: {
            a: 'Внешние размеры (Д x Ш x В, см) 27 x 11,5 x 23',
            b: 'Внутренние размеры (Д x Ш x В, см) 17,5 x 11 x 16,5',
            c: 'Вес (кг) 1,1'
          },
          colors: {
            back: 1,
            red: 1,
            blue: 1,
            white: 1,
            green: 1
          },
        },
        {
          id: 100004,
          name: "Runtastic RUNBT1 Heart Rate Combo Monitor", 
          url: "#runtastic_runbt1_heart_rate_combo_monitor",
          desc: "Пульсометр может использоваться во время бега, занятий греблей, занятий боевыми искусствами и йогой, а также для всех других видов спорта. ",
          model: "Пульсометр",
          rating_numb: 4,
          price: 7,
          src_photo: "img/img.71.jpg",
          inbasket: 0,
           brand: 'Runtastic',
          isnew: 1,
          delivery_days: 12,
          awards: {
            reddot: 0,
            eisa: 0,
            tipa: 0
          },
          properties: {
            a: 'Bluetooth Смарт Ready для (Bluetooth 4.0) подключения,совместимость сустройствами IOS',
            b: 'iPhone 4S и выше, Android 4.3 ( +Samsung Galaxy S3)',
            c: '25.5 х 9.4 х 16.5 см ; 164 гр. (вес при пересылке – 181 гр.) Пластиковый корпус, тканевый ремешок',
            d: '1 литий-ионная батарейка (в комплекте)'
          },
          colors: {
            back: 1
          },
        },
      ]);
      return goods.models;
    };

    var API = {
      getGoods: function() {
        var goods = new Entities.GoodCollection();
        var defer = $.Deferred();
        goods.fetch({
          success: function(data) {
            defer.resolve(data);
          },
          error: function(data) {
            defer.resolve(data);
          }
        });
        var promise = defer.promise();
        $.when(promise).done(function(goods) {
          if (goods.length === 0) {
            var models = initializeGoods();
            goods.reset(models);
          }
        });
        return promise;
        }
    };

    ShopManager.reqres.setHandler("good:entities", function() {
      return API.getGoods();
    });
  });
  return ;
});
