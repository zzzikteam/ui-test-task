requirejs.config({
  paths: {
    backbone: "vendor/backbone",
    "backbone.picky": "vendor/backbone.picky",
    "backbone.syphon": "vendor/backbone.syphon",
    jquery: "vendor/jquery",
    "jquery-ui": "vendor/jquery-ui",
    json2: "vendor/json2",
    marionette: "vendor/backbone.marionette",
    "marionette.tuner": "vendor/marionette.tuner",
    spin: "vendor/spin",
    "spin.jquery": "vendor/spin.jquery",
    tpl: "vendor/tpl",
    underscore: "vendor/underscore",
    yr: "vendor/runtime"
  },

  shim: {
    underscore: {
      exports: "_"
    },
    backbone: {
      deps: ["jquery", "underscore", "json2"],
      exports: "Backbone"
    },
    "backbone.picky": ["backbone"],
    "backbone.syphon": ["backbone"],
    marionette: {
      deps: ["backbone"],
      exports: "Marionette"
    },
    "marionette.tuner": ["marionette"],
    "jquery-ui": ["jquery"],
    "spin.jquery": ["spin", "jquery"],
    yr: {
       exports: "yr"
    }
  }
});

require(["app"], function(ShopManager) {
  ShopManager.start();
});
