define(["marionette", "yr"], function(Marionette) {
  var ShopManager = new Marionette.Application();

  ShopManager.addRegions({
    headerRegion: "#header-region",
    mainRegion: "#content",
    footerRegion: "#footer-region",
    popupRegion: "#popup-region"
  });

  ShopManager.navigate = function(route, options) {
    options || (options = {});
    Backbone.history.navigate(route, options);
  };

  ShopManager.getCurrentRoute = function() {
    return Backbone.history.fragment;
  };

  ShopManager.on("initialize:after", function() {
    if (Backbone.history) {
		require(["apps/goods/goods_app"], function() {
        Backbone.history.start();

        if (ShopManager.getCurrentRoute() === "") {
          ShopManager.trigger("goods:list");
        }
      });
    }
  });

  return ShopManager;
});
